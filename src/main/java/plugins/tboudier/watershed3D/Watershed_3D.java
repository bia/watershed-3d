package plugins.tboudier.watershed3D;

import icy.image.IcyBufferedImage;
import icy.plugin.PluginDescriptor;
import icy.plugin.PluginInstaller;
import icy.plugin.PluginLoader;
import icy.sequence.Sequence;
import icy.sequence.VolumetricImage;
import icy.type.DataType;
import icy.type.collection.array.ArrayUtil;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageInt;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.processing.FastFilters3D;
import mcib3d.image3d.regionGrowing.Watershed3D;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarSequence;

public class Watershed_3D extends EzPlug implements Block
{
    private final EzVarDouble EZVarRad = new EzVarDouble("Radius", 2.0D, 0.0D, 100.0D, 0.5D);
    private final EzVarSequence EZSequence = new EzVarSequence("Signal");
    private final EzVarSequence EZSequence2 = new EzVarSequence("Seeds");
    private final EzVarSequence EZSequenceOut = new EzVarSequence("Watershed");
    private final EzVarDouble EZVarThSignal = new EzVarDouble("Threshold signal", 10.0D, 0.0D, 65535.0D, 1.0D);
    private final EzVarDouble EZVarThSeeds = new EzVarDouble("Threshold seeds", 100.0D, 0.0D, 65535.0D, 1.0D);

    @Override
    protected void initialize()
    {
        this.EZVarRad.setToolTipText(
                "The radius is used to compute seeds automatically as local maxima of the image, when no sequence is selected for seeds.");
        this.EZSequence.setToolTipText("The signal image to be processed by watershed.");
        this.EZSequence2.setToolTipText(
                "The image containing the seeds for the watershed, if no sequence is selected, local maxima will be computed as seeds.");
        this.EZVarThSignal.setToolTipText(
                "Only the values from the signal image above this threshold will be processed by watershed");
        this.EZVarThSeeds
                .setToolTipText("Only the values from the seeds image above this threshold will be used as seeds");

        super.addEzComponent(this.EZSequence);
        super.addEzComponent(this.EZSequence2);
        super.addEzComponent(this.EZVarThSignal);
        super.addEzComponent(this.EZVarThSeeds);
        super.addEzComponent(this.EZVarRad);
    }

    @Override
    protected void execute()
    {
        // Stephane: try to desinstall the olds annoying version of the plugin silently
        PluginDescriptor plugin = PluginLoader.getPlugin("plugins.tboudier.watershed3D.WatershedSeeded3D");
        if (plugin != null)
            PluginInstaller.desinstall(plugin, false, false);
        plugin = PluginLoader.getPlugin("plugins.tboudier.watershed3D.Watershed3D");
        if (plugin != null)
            PluginInstaller.desinstall(plugin, false, false);
        
        Sequence signal = this.EZSequence.getValue();

        DataType type = signal.getDataType_();
        int nbSlice = signal.getSizeZ();
        int nbPixel = signal.getSizeX() * signal.getSizeY();
        boolean gui = !isHeadLess();
        ImageHandler signalImg;

        switch (type)
        {
            case USHORT:
                signalImg = new ImageShort(signal.getDataXYZAsShort(0, 0), "tmp", signal.getWidth());
                break;
            case UBYTE:
                signalImg = new ImageByte(signal.getDataXYZAsByte(0, 0), "tmp", signal.getWidth());
                break;
            case FLOAT:
                signalImg = new ImageFloat(signal.getDataXYZAsFloat(0, 0), "tmp", signal.getWidth());
                break;
            default:
                float[][] arrayImg = new float[nbSlice][nbPixel];

                System.out.println("basic type " + type + " not supported; converting to float");
                for (int z = 0; z < nbSlice; z++)
                    arrayImg[z] = ((float[]) ArrayUtil.arrayToFloatArray(signal.getDataXY(0, z, 0), type.isSigned()));

                signalImg = new ImageFloat(arrayImg, "tmp", signal.getWidth());
                break;
        }

        Sequence seeds = this.EZSequence2.getValue();
        ImageHandler seedsImg;

        if (seeds != null)
        {
            type = seeds.getDataType_();
            switch (type)
            {
                case USHORT:
                    seedsImg = new ImageShort(seeds.getDataXYZAsShort(0, 0), "tmp", signal.getWidth());
                    break;
                case UBYTE:
                    seedsImg = new ImageByte(seeds.getDataXYZAsByte(0, 0), "tmp", signal.getWidth());
                    break;
                case FLOAT:
                    seedsImg = new ImageFloat(seeds.getDataXYZAsFloat(0, 0), "tmp", signal.getWidth());
                    break;
                default:
                    float[][] arrayImg = new float[nbSlice][nbPixel];

                    System.out.println("basic type " + type + " not supported; converting to float");
                    for (int z = 0; z < nbSlice; z++)
                        arrayImg[z] = ((float[]) ArrayUtil.arrayToFloatArray(seeds.getDataXY(0, z, 0),
                                type.isSigned()));

                    seedsImg = new ImageFloat(arrayImg, "tmp", signal.getWidth());
                    break;
            }
        }
        else
        {
            seedsImg = FastFilters3D.filterImage(signalImg, 4, this.EZVarRad.getValue().floatValue(),
                    this.EZVarRad.getValue().floatValue(), this.EZVarRad.getValue().floatValue(), 0, false);
        }

        Watershed3D water = new Watershed3D(signalImg, seedsImg, this.EZVarThSignal.getValue().intValue(),
                this.EZVarThSeeds.getValue().intValue());
        ImageInt res = water.getWatershedImage3D();

        Sequence seqOut = new Sequence();
        VolumetricImage vol = seqOut.addVolumetricImage();
        createVolumeFromImageHandler(res, vol);
        seqOut.setName("Watershed3D");
        seqOut.setPixelSizeX(signal.getPixelSizeX());
        seqOut.setPixelSizeY(signal.getPixelSizeY());
        seqOut.setPixelSizeZ(signal.getPixelSizeZ());
        seqOut.dataChanged();

        if (gui)
            addSequence(seqOut);

        this.EZSequenceOut.setValue(seqOut);
    }

    private static void createVolumeFromImageHandler(ImageHandler res, VolumetricImage vol)
    {
        for (int z = 0; z < res.sizeZ; z++)
        {
            short[] tabres = (short[]) res.getArray1D(z);
            IcyBufferedImage icyimg = new IcyBufferedImage(res.sizeX, res.sizeY, tabres);
            vol.setImage(z, icyimg);
        }
    }

    @Override
    public void clean()
    {
        //
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Signal", this.EZSequence.getVariable());
        inputMap.add("Seeds", this.EZSequence2.getVariable());
        inputMap.add("Threshold Signal", this.EZVarThSignal.getVariable());
        inputMap.add("Threshold seeds", this.EZVarThSeeds.getVariable());
        inputMap.add("Radius", this.EZVarRad.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("watershed", this.EZSequenceOut.getVariable());
    }
}
